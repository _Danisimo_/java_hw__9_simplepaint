package sample;

import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;

import javax.imageio.ImageIO;
import java.io.File;

public class Controller {


    @FXML
    private Canvas canvas;

    @FXML
    private ColorPicker colorPicker;

    @FXML
    private CheckBox eraser;

    @FXML
    private Slider  drawSize;

    public void onSave(){
        try{
            Image snapshot = canvas.snapshot(null,null);
            ImageIO.write(SwingFXUtils.fromFXImage(snapshot,null),"png",  new File("paint.png"));
        }catch(Exception e ){
            System.out.println("Error" + e);
        }
    }

    public void onExit(){
        Platform.exit();
    }

    public void initialize(){
        GraphicsContext g = canvas.getGraphicsContext2D();
        canvas.setOnMouseDragged(e->{
            double size = drawSize.getValue();
            double x = e.getX();
            double y = e.getY();

           if (eraser.isSelected()){
                g.clearRect(x,y,size,size);
            }else{
               g.setFill(colorPicker.getValue());
                g.fillRect(x,y,size,size);
            }
        });
    }

}
